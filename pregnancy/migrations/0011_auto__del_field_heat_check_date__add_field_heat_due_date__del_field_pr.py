# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Heat.check_date'
        db.delete_column(u'pregnancy_heat', 'check_date')

        # Adding field 'Heat.due_date'
        db.add_column(u'pregnancy_heat', 'due_date',
                      self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2015, 1, 20, 0, 0)),
                      keep_default=False)

        # Deleting field 'Pregnant.check_date'
        db.delete_column(u'pregnancy_pregnant', 'check_date')

        # Adding field 'Pregnant.due_date'
        db.add_column(u'pregnancy_pregnant', 'due_date',
                      self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2015, 1, 20, 0, 0)),
                      keep_default=False)

        # Deleting field 'Parturition.check_date'
        db.delete_column(u'pregnancy_parturition', 'check_date')

        # Adding field 'Parturition.due_date'
        db.add_column(u'pregnancy_parturition', 'due_date',
                      self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2015, 1, 20, 0, 0)),
                      keep_default=False)

        # Deleting field 'Treatment.check_date'
        db.delete_column(u'pregnancy_treatment', 'check_date')

        # Adding field 'Treatment.due_date'
        db.add_column(u'pregnancy_treatment', 'due_date',
                      self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2015, 1, 20, 0, 0)),
                      keep_default=False)

        # Deleting field 'Conceieved.check_date'
        db.delete_column(u'pregnancy_conceieved', 'check_date')

        # Adding field 'Conceieved.due_date'
        db.add_column(u'pregnancy_conceieved', 'due_date',
                      self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2015, 1, 20, 0, 0)),
                      keep_default=False)


    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'Heat.check_date'
        raise RuntimeError("Cannot reverse this migration. 'Heat.check_date' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Heat.check_date'
        db.add_column(u'pregnancy_heat', 'check_date',
                      self.gf('django.db.models.fields.DateField')(),
                      keep_default=False)

        # Deleting field 'Heat.due_date'
        db.delete_column(u'pregnancy_heat', 'due_date')


        # User chose to not deal with backwards NULL issues for 'Pregnant.check_date'
        raise RuntimeError("Cannot reverse this migration. 'Pregnant.check_date' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Pregnant.check_date'
        db.add_column(u'pregnancy_pregnant', 'check_date',
                      self.gf('django.db.models.fields.DateField')(),
                      keep_default=False)

        # Deleting field 'Pregnant.due_date'
        db.delete_column(u'pregnancy_pregnant', 'due_date')


        # User chose to not deal with backwards NULL issues for 'Parturition.check_date'
        raise RuntimeError("Cannot reverse this migration. 'Parturition.check_date' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Parturition.check_date'
        db.add_column(u'pregnancy_parturition', 'check_date',
                      self.gf('django.db.models.fields.DateField')(),
                      keep_default=False)

        # Deleting field 'Parturition.due_date'
        db.delete_column(u'pregnancy_parturition', 'due_date')


        # User chose to not deal with backwards NULL issues for 'Treatment.check_date'
        raise RuntimeError("Cannot reverse this migration. 'Treatment.check_date' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Treatment.check_date'
        db.add_column(u'pregnancy_treatment', 'check_date',
                      self.gf('django.db.models.fields.DateField')(),
                      keep_default=False)

        # Deleting field 'Treatment.due_date'
        db.delete_column(u'pregnancy_treatment', 'due_date')


        # User chose to not deal with backwards NULL issues for 'Conceieved.check_date'
        raise RuntimeError("Cannot reverse this migration. 'Conceieved.check_date' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Conceieved.check_date'
        db.add_column(u'pregnancy_conceieved', 'check_date',
                      self.gf('django.db.models.fields.DateField')(),
                      keep_default=False)

        # Deleting field 'Conceieved.due_date'
        db.delete_column(u'pregnancy_conceieved', 'due_date')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'cow.cowprofile': {
            'Meta': {'object_name': 'CowProfile'},
            'breed': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'dairy': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cow.Dairy']"}),
            'date_of_birth': ('django.db.models.fields.DateField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        'cow.dairy': {
            'Meta': {'object_name': 'Dairy'},
            'address': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'pregnancy.conceieved': {
            'Meta': {'object_name': 'Conceieved'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'due_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notes': ('django.db.models.fields.TextField', [], {}),
            'pregnancy': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['pregnancy.Pregnancy']"}),
            'result': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        },
        u'pregnancy.heat': {
            'Meta': {'object_name': 'Heat'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'due_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notes': ('django.db.models.fields.TextField', [], {}),
            'pregnancy': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['pregnancy.Pregnancy']"}),
            'result': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        },
        u'pregnancy.parturition': {
            'Meta': {'object_name': 'Parturition'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'due_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notes': ('django.db.models.fields.TextField', [], {}),
            'pregnancy': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['pregnancy.Pregnancy']"}),
            'result': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        },
        'pregnancy.pregnancy': {
            'Meta': {'object_name': 'Pregnancy'},
            'added_on': ('django.db.models.fields.DateField', [], {}),
            'cow': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cow.CowProfile']"}),
            'cycle_completed': ('django.db.models.fields.BooleanField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'pregnancy.pregnant': {
            'Meta': {'object_name': 'Pregnant'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'due_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notes': ('django.db.models.fields.TextField', [], {}),
            'pregnancy': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['pregnancy.Pregnancy']"}),
            'result': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        },
        u'pregnancy.treatment': {
            'Meta': {'object_name': 'Treatment'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'due_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'notes': ('django.db.models.fields.TextField', [], {}),
            'pregnancy': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['pregnancy.Pregnancy']"}),
            'result': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        }
    }

    complete_apps = ['pregnancy']