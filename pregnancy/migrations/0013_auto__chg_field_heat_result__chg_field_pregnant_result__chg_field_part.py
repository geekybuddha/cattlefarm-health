# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Heat.result'
        db.alter_column(u'pregnancy_heat', 'result', self.gf('django.db.models.fields.CharField')(max_length=15, null=True))

        # Changing field 'Pregnant.result'
        db.alter_column(u'pregnancy_pregnant', 'result', self.gf('django.db.models.fields.CharField')(max_length=15, null=True))

        # Changing field 'Parturition.result'
        db.alter_column(u'pregnancy_parturition', 'result', self.gf('django.db.models.fields.CharField')(max_length=15, null=True))

        # Changing field 'Treatment.result'
        db.alter_column(u'pregnancy_treatment', 'result', self.gf('django.db.models.fields.CharField')(max_length=15, null=True))

        # Changing field 'Conceieved.result'
        db.alter_column(u'pregnancy_conceieved', 'result', self.gf('django.db.models.fields.CharField')(max_length=15, null=True))

    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'Heat.result'
        raise RuntimeError("Cannot reverse this migration. 'Heat.result' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration
        # Changing field 'Heat.result'
        db.alter_column(u'pregnancy_heat', 'result', self.gf('django.db.models.fields.CharField')(max_length=15))

        # User chose to not deal with backwards NULL issues for 'Pregnant.result'
        raise RuntimeError("Cannot reverse this migration. 'Pregnant.result' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration
        # Changing field 'Pregnant.result'
        db.alter_column(u'pregnancy_pregnant', 'result', self.gf('django.db.models.fields.CharField')(max_length=15))

        # User chose to not deal with backwards NULL issues for 'Parturition.result'
        raise RuntimeError("Cannot reverse this migration. 'Parturition.result' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration
        # Changing field 'Parturition.result'
        db.alter_column(u'pregnancy_parturition', 'result', self.gf('django.db.models.fields.CharField')(max_length=15))

        # User chose to not deal with backwards NULL issues for 'Treatment.result'
        raise RuntimeError("Cannot reverse this migration. 'Treatment.result' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration
        # Changing field 'Treatment.result'
        db.alter_column(u'pregnancy_treatment', 'result', self.gf('django.db.models.fields.CharField')(max_length=15))

        # User chose to not deal with backwards NULL issues for 'Conceieved.result'
        raise RuntimeError("Cannot reverse this migration. 'Conceieved.result' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration
        # Changing field 'Conceieved.result'
        db.alter_column(u'pregnancy_conceieved', 'result', self.gf('django.db.models.fields.CharField')(max_length=15))

    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'cow.cowprofile': {
            'Meta': {'object_name': 'CowProfile'},
            'breed': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'dairy': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cow.Dairy']"}),
            'date_of_birth': ('django.db.models.fields.DateField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        'cow.dairy': {
            'Meta': {'object_name': 'Dairy'},
            'address': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        'cow.healthtask': {
            'Meta': {'object_name': 'HealthTask'},
            'added_on': ('django.db.models.fields.DateField', [], {}),
            'completed_on': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            'cow': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cow.CowProfile']"}),
            'due_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'parent_task': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cow.HealthTask']", 'null': 'True'}),
            'result': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'task_completed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'task_details': ('django.db.models.fields.TextField', [], {}),
            'task_name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'pregnancy.conceieved': {
            'Meta': {'object_name': 'Conceieved'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {}),
            'completed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'due_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notes': ('django.db.models.fields.TextField', [], {}),
            'pregnancy': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['pregnancy.Pregnancy']"}),
            'result': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True'})
        },
        u'pregnancy.heat': {
            'Meta': {'object_name': 'Heat'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {}),
            'completed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'due_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notes': ('django.db.models.fields.TextField', [], {}),
            'pregnancy': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['pregnancy.Pregnancy']"}),
            'result': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'})
        },
        u'pregnancy.parturition': {
            'Meta': {'object_name': 'Parturition'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {}),
            'completed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'due_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notes': ('django.db.models.fields.TextField', [], {}),
            'pregnancy': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['pregnancy.Pregnancy']"}),
            'result': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True'})
        },
        'pregnancy.pregnancy': {
            'Meta': {'object_name': 'Pregnancy'},
            'added_on': ('django.db.models.fields.DateField', [], {}),
            'cow': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cow.CowProfile']"}),
            'cycle_completed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'pregnancy.pregnant': {
            'Meta': {'object_name': 'Pregnant'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {}),
            'completed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'due_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notes': ('django.db.models.fields.TextField', [], {}),
            'pregnancy': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['pregnancy.Pregnancy']"}),
            'result': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True'})
        },
        u'pregnancy.treatment': {
            'Meta': {'object_name': 'Treatment'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {}),
            'completed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'due_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'notes': ('django.db.models.fields.TextField', [], {}),
            'pregnancy': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['pregnancy.Pregnancy']"}),
            'result': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True'})
        }
    }

    complete_apps = ['pregnancy']