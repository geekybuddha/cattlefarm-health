# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Heat'
        db.create_table(u'pregnancy_heat', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('pregnancy', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['pregnancy.Pregnancy'])),
            ('date', self.gf('django.db.models.fields.DateField')()),
            ('notes', self.gf('django.db.models.fields.TextField')()),
            ('result', self.gf('django.db.models.fields.CharField')(max_length=15)),
        ))
        db.send_create_signal(u'pregnancy', ['Heat'])

        # Adding model 'Pregnant'
        db.create_table(u'pregnancy_pregnant', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('pregnancy', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['pregnancy.Pregnancy'])),
            ('date', self.gf('django.db.models.fields.DateField')()),
            ('notes', self.gf('django.db.models.fields.TextField')()),
            ('result', self.gf('django.db.models.fields.CharField')(max_length=15)),
        ))
        db.send_create_signal(u'pregnancy', ['Pregnant'])

        # Adding model 'Parturition'
        db.create_table(u'pregnancy_parturition', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('pregnancy', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['pregnancy.Pregnancy'])),
            ('date', self.gf('django.db.models.fields.DateField')()),
            ('notes', self.gf('django.db.models.fields.TextField')()),
            ('result', self.gf('django.db.models.fields.CharField')(max_length=15)),
        ))
        db.send_create_signal(u'pregnancy', ['Parturition'])

        # Adding model 'Coneieved'
        db.create_table(u'pregnancy_coneieved', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('pregnancy', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['pregnancy.Pregnancy'])),
            ('date', self.gf('django.db.models.fields.DateField')()),
            ('notes', self.gf('django.db.models.fields.TextField')()),
            ('result', self.gf('django.db.models.fields.CharField')(max_length=15)),
        ))
        db.send_create_signal(u'pregnancy', ['Coneieved'])

        # Adding model 'Treatment'
        db.create_table(u'pregnancy_treatment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('pregnancy', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['pregnancy.Pregnancy'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('date', self.gf('django.db.models.fields.DateField')()),
            ('notes', self.gf('django.db.models.fields.TextField')()),
            ('result', self.gf('django.db.models.fields.CharField')(max_length=15)),
        ))
        db.send_create_signal(u'pregnancy', ['Treatment'])

        # Deleting field 'Pregnancy.due_date'
        db.delete_column(u'pregnancy_pregnancy', 'due_date')

        # Deleting field 'Pregnancy.previous_date'
        db.delete_column(u'pregnancy_pregnancy', 'previous_date')

        # Deleting field 'Pregnancy.pregnant_date'
        db.delete_column(u'pregnancy_pregnancy', 'pregnant_date')

        # Deleting field 'Pregnancy.repeat_date'
        db.delete_column(u'pregnancy_pregnancy', 'repeat_date')

        # Deleting field 'Pregnancy.notes'
        db.delete_column(u'pregnancy_pregnancy', 'notes')

        # Deleting field 'Pregnancy.parturition_date'
        db.delete_column(u'pregnancy_pregnancy', 'parturition_date')

        # Deleting field 'Pregnancy.heat_date'
        db.delete_column(u'pregnancy_pregnancy', 'heat_date')

        # Deleting field 'Pregnancy.previous_stage'
        db.delete_column(u'pregnancy_pregnancy', 'previous_stage')

        # Deleting field 'Pregnancy.birth_date'
        db.delete_column(u'pregnancy_pregnancy', 'birth_date')

        # Deleting field 'Pregnancy.conceived_date'
        db.delete_column(u'pregnancy_pregnancy', 'conceived_date')

        # Deleting field 'Pregnancy.stage'
        db.delete_column(u'pregnancy_pregnancy', 'stage')


    def backwards(self, orm):
        # Deleting model 'Heat'
        db.delete_table(u'pregnancy_heat')

        # Deleting model 'Pregnant'
        db.delete_table(u'pregnancy_pregnant')

        # Deleting model 'Parturition'
        db.delete_table(u'pregnancy_parturition')

        # Deleting model 'Coneieved'
        db.delete_table(u'pregnancy_coneieved')

        # Deleting model 'Treatment'
        db.delete_table(u'pregnancy_treatment')


        # User chose to not deal with backwards NULL issues for 'Pregnancy.due_date'
        raise RuntimeError("Cannot reverse this migration. 'Pregnancy.due_date' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Pregnancy.due_date'
        db.add_column(u'pregnancy_pregnancy', 'due_date',
                      self.gf('django.db.models.fields.DateField')(),
                      keep_default=False)

        # Adding field 'Pregnancy.previous_date'
        db.add_column(u'pregnancy_pregnancy', 'previous_date',
                      self.gf('django.db.models.fields.DateField')(null=True),
                      keep_default=False)

        # Adding field 'Pregnancy.pregnant_date'
        db.add_column(u'pregnancy_pregnancy', 'pregnant_date',
                      self.gf('django.db.models.fields.DateField')(null=True),
                      keep_default=False)

        # Adding field 'Pregnancy.repeat_date'
        db.add_column(u'pregnancy_pregnancy', 'repeat_date',
                      self.gf('django.db.models.fields.DateField')(null=True),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Pregnancy.notes'
        raise RuntimeError("Cannot reverse this migration. 'Pregnancy.notes' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Pregnancy.notes'
        db.add_column(u'pregnancy_pregnancy', 'notes',
                      self.gf('django.db.models.fields.TextField')(),
                      keep_default=False)

        # Adding field 'Pregnancy.parturition_date'
        db.add_column(u'pregnancy_pregnancy', 'parturition_date',
                      self.gf('django.db.models.fields.DateField')(null=True),
                      keep_default=False)

        # Adding field 'Pregnancy.heat_date'
        db.add_column(u'pregnancy_pregnancy', 'heat_date',
                      self.gf('django.db.models.fields.DateField')(null=True),
                      keep_default=False)

        # Adding field 'Pregnancy.previous_stage'
        db.add_column(u'pregnancy_pregnancy', 'previous_stage',
                      self.gf('django.db.models.fields.CharField')(max_length=15, null=True),
                      keep_default=False)

        # Adding field 'Pregnancy.birth_date'
        db.add_column(u'pregnancy_pregnancy', 'birth_date',
                      self.gf('django.db.models.fields.DateField')(null=True),
                      keep_default=False)

        # Adding field 'Pregnancy.conceived_date'
        db.add_column(u'pregnancy_pregnancy', 'conceived_date',
                      self.gf('django.db.models.fields.DateField')(null=True),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Pregnancy.stage'
        raise RuntimeError("Cannot reverse this migration. 'Pregnancy.stage' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Pregnancy.stage'
        db.add_column(u'pregnancy_pregnancy', 'stage',
                      self.gf('django.db.models.fields.CharField')(max_length=15),
                      keep_default=False)


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'cow.cowprofile': {
            'Meta': {'object_name': 'CowProfile'},
            'breed': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'dairy': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cow.Dairy']"}),
            'date_of_birth': ('django.db.models.fields.DateField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        'cow.dairy': {
            'Meta': {'object_name': 'Dairy'},
            'address': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'pregnancy.coneieved': {
            'Meta': {'object_name': 'Coneieved'},
            'date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notes': ('django.db.models.fields.TextField', [], {}),
            'pregnancy': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['pregnancy.Pregnancy']"}),
            'result': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        },
        u'pregnancy.heat': {
            'Meta': {'object_name': 'Heat'},
            'date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notes': ('django.db.models.fields.TextField', [], {}),
            'pregnancy': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['pregnancy.Pregnancy']"}),
            'result': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        },
        u'pregnancy.parturition': {
            'Meta': {'object_name': 'Parturition'},
            'date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notes': ('django.db.models.fields.TextField', [], {}),
            'pregnancy': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['pregnancy.Pregnancy']"}),
            'result': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        },
        'pregnancy.pregnancy': {
            'Meta': {'object_name': 'Pregnancy'},
            'added_on': ('django.db.models.fields.DateField', [], {}),
            'cow': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cow.CowProfile']"}),
            'cycle_completed': ('django.db.models.fields.BooleanField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'pregnancy.pregnant': {
            'Meta': {'object_name': 'Pregnant'},
            'date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notes': ('django.db.models.fields.TextField', [], {}),
            'pregnancy': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['pregnancy.Pregnancy']"}),
            'result': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        },
        u'pregnancy.treatment': {
            'Meta': {'object_name': 'Treatment'},
            'date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'notes': ('django.db.models.fields.TextField', [], {}),
            'pregnancy': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['pregnancy.Pregnancy']"}),
            'result': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        }
    }

    complete_apps = ['pregnancy']