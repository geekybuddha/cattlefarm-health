from django.db import models
from cow.models import CowProfile
from django.utils.translation import ugettext_lazy as _

class Pregnancy(models.Model):
	cow = models.ForeignKey(CowProfile)
	added_on = models.DateField(_('added on'))
	
	cycle_completed = models.BooleanField(_('cycle completed'))

	class Meta:
                app_label = 'pregnancy'
		verbose_name = _('Pregnancy')
	

        def __unicode__(self):
                return '%s,%s' % (self.cow,self.added_on)

class Parturition(models.Model):
	pregnancy = models.ForeignKey(Pregnancy)
	date = models.DateField(_('date'))
	notes = models.TextField(_('notes'))
	CHOICES = (
	('SUCCESS','Success'),
	('FAILED','Failed'),
	)

	added_on = models.DateTimeField(_('added on'))
	due_date = models.DateField(_('due date'))
	result = models.CharField(_('result'),max_length=15,choices=CHOICES,null=True,blank=True) 
	completed = models.BooleanField(_('completed'),default=False)
	def __unicode__(self):
                return 'Parturition'
	class Meta:
		verbose_name = _('Parturition')

class Heat(models.Model):
	pregnancy = models.ForeignKey(Pregnancy)
	date = models.DateField(_('date'))
	notes = models.TextField(_('notes'))
	CHOICES = (
	('EMPTY','Empty'),
	('CONCEIVED','Conceieved'),
	)

	completed = models.BooleanField(_('completed'),default=False)
	added_on = models.DateTimeField(_('added on'))
	due_date = models.DateField(_('due date'))
	result = models.CharField(_('result'),max_length=15,choices=CHOICES,null=True,blank=True) 
	def __unicode__(self):
                return 'Heat'

	class Meta:
		verbose_name = _('Heat')

class Conceieved(models.Model):
	pregnancy = models.ForeignKey(Pregnancy)
	date = models.DateField(_('date'))
	notes = models.TextField(_('notes'))
	CHOICES = (
	('REPEAT','Repeat'),
	('PREGNANT','Pregnant'),
	)

	completed = models.BooleanField(_('completed'),default=False)
	added_on = models.DateTimeField(_('added on'))
	due_date = models.DateField(_('due date'))
	result = models.CharField(_('result'),max_length=15,choices=CHOICES,null=True,blank=True) 
	def __unicode__(self):
                return 'Coneieved'

	class Meta:
		verbose_name = _('Conceieved')

class Pregnant(models.Model):
	pregnancy = models.ForeignKey(Pregnancy)
	date = models.DateField(_('date'))
	notes = models.TextField(_('notes'))
	CHOICES = (
	('SUCCESS','Success'),
	('FAILED','Failed'),
	)

	completed = models.BooleanField(_('completed'),default=False)
	added_on = models.DateTimeField(_('added on'))
	due_date = models.DateField(_('due date'))
	result = models.CharField(_('result'),max_length=15,choices=CHOICES,null=True,blank=True) 
	def __unicode__(self):
                return 'Pregnant'

	class Meta:
		verbose_name = _('Pregnant')

class Treatment(models.Model):
	pregnancy = models.ForeignKey(Pregnancy)
	name = models.CharField(_('name'),max_length=100)
	date = models.DateField(_('date'))
	notes = models.TextField(_('notes'))
	CHOICES = (
	('SUCCESS','Success'),
	('FAILED','Failed'),
	)

	completed = models.BooleanField(_('completed'),default=False)
	added_on = models.DateTimeField(_('added on'))
	due_date = models.DateField(_('due date'))
	result = models.CharField(_('result'),max_length=15,choices=CHOICES,null=True,blank=True) 

	def __unicode__(self):
                return 'Treatment'

	class Meta:
		verbose_name = _('Treatment')
