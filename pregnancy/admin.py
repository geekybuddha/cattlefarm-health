from django.contrib import admin

from pregnancy.models import *

admin.site.register(Pregnancy)
admin.site.register(Heat)
admin.site.register(Conceieved)
admin.site.register(Pregnant)
admin.site.register(Treatment)

