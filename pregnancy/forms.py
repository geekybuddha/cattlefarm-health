from django.forms import ModelForm
from pregnancy.models import *
from django import forms
from cow.models import CowProfile
class PregnancyForm(forms.Form):
    	pregnancy = forms.ModelChoiceField(queryset=None)
    	def __init__(self, dairy, *args, **kwargs):
        	super(PregnancyForm, self).__init__(*args, **kwargs)
        	self.fields['pregnancy'].queryset=Pregnancy.objects.filter(cow__dairy=dairy)

class NewPregnancyForm(ModelForm):
	class Meta:
		model = Pregnancy
		exclude = ('cycle_completed','added_on',)
    	def __init__(self, dairy, *args, **kwargs):
        	super(NewPregnancyForm, self).__init__(*args, **kwargs)
        	self.fields['cow'].queryset=CowProfile.objects.filter(dairy=dairy)

class HeatForm(ModelForm):
	class Meta:
		model = Heat
		exclude = ('pregnancy','added_on','result','completed')
class ConceievedForm(ModelForm):
	class Meta:
		model = Conceieved
		exclude = ('pregnancy','added_on','result','completed')
class ParturitionForm(ModelForm):
	class Meta:
		model = Parturition
		exclude = ('pregnancy','added_on','result','completed')

class PregnantForm(ModelForm):
	class Meta:
		model = Pregnant
		exclude = ('pregnancy','added_on','result','completed')

class TreatmentForm(ModelForm):
	class Meta:
		model = Treatment
		exclude = ('pregnancy','added_on','result','completed')


class HeatResultForm(ModelForm):
	class Meta:
		model = Heat
		exclude = ('pregnancy','added_on','date','due_date','notes')
class ConeievedResultForm(ModelForm):
	class Meta:
		model = Conceieved
		exclude = ('pregnancy','added_on','date','due_date','notes')
class ParturitionResultForm(ModelForm):
	class Meta:
		model = Parturition
		exclude = ('pregnancy','added_on','date','due_date','notes')

class PregnantResultForm(ModelForm):
	class Meta:
		model = Pregnant
		exclude = ('pregnancy','added_on','date','due_date','notes')

class TreatmentResultForm(ModelForm):
	class Meta:
		model = Treatment
		exclude = ('pregnancy','added_on','date','due_date','notes')
