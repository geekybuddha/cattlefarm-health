from django.shortcuts import render_to_response
from pregnancy.models import Pregnancy
from pregnancy.forms import *
from datetime import datetime
from django.http import HttpResponse,HttpResponseRedirect
from django.template import RequestContext, loader
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from django.http import Http404
from cow.models import UserDairy
from operator import attrgetter
from itertools import chain

@login_required
def index(request):
	dairy = UserDairy.objects.get(user=request.user).dairy	
	heat_list = Heat.objects.filter(pregnancy__cow__dairy=dairy,completed=False)
	conceieved_list = Conceieved.objects.filter(pregnancy__cow__dairy=dairy,completed=False)
	parturition_list = Parturition.objects.filter(pregnancy__cow__dairy=dairy,completed=False)
	pregnant_list = Pregnant.objects.filter(pregnancy__cow__dairy=dairy,completed=False)
	
	form = PregnancyForm(dairy=dairy)
	if request.method == 'POST':
		form = PregnancyForm(data=request.POST,dairy=dairy)
		if form.is_valid():
			pregnancy=form.cleaned_data['pregnancy']
			heat_list = Heat.objects.filter(pregnancy=form.cleaned_data['pregnancy'])
			conceieved_list = Conceieved.objects.filter(pregnancy=form.cleaned_data['pregnancy'])
			parturition_list = Parturition.objects.filter(pregnancy=form.cleaned_data['pregnancy'])
			pregnant_list = Pregnant.objects.filter(pregnancy=form.cleaned_data['pregnancy'])
			result_list = sorted(chain(heat_list, conceieved_list, parturition_list,pregnant_list),key=attrgetter('date'))
			url = "/pregnancy/record/%s/" % (pregnancy.id)
			messages.add_message(request, messages.INFO, 'New reproduction record added!')
			return HttpResponseRedirect(url)

	return render_to_response('pregnancy/index.html',
                          {'heat_list':heat_list,'conceieved_list':conceieved_list,'parturated_list':parturition_list,'pregnant_list':pregnant_list,'form':form},context_instance=RequestContext(request))
@login_required
def view_record(request,pregnancy_id):
	pregnancy = Pregnancy.objects.get(pk=pregnancy_id)
	heat_list = Heat.objects.filter(pregnancy=pregnancy)
	conceieved_list = Conceieved.objects.filter(pregnancy=pregnancy)
	parturition_list = Parturition.objects.filter(pregnancy=pregnancy)
	pregnant_list = Pregnant.objects.filter(pregnancy=pregnancy)
	treatment_list = Treatment.objects.filter(pregnancy=pregnancy)
	result_list = sorted(chain(treatment_list,heat_list, conceieved_list, parturition_list,pregnant_list),key=attrgetter('date'))
	return render_to_response('pregnancy/record.html',{'pregnancy':pregnancy,'result_list':result_list},context_instance=RequestContext(request))



@login_required
def add_record(request):

	dairy = UserDairy.objects.get(user=request.user).dairy	
	form = NewPregnancyForm(dairy)

	if request.method == 'POST':
        	form = NewPregnancyForm(data=request.POST, files=request.FILES,dairy=dairy)
        	if form.is_valid():
  			task = form.save(commit=False)
			task.added_on = datetime.now()
			task.cycle_completed = False 
			task.save()
			messages.add_message(request, messages.INFO, 'Pregnancy record added successfully')
			url = "/pregnancy/record/%s/" % (task.id)
			return HttpResponseRedirect(url)


	return render_to_response('pregnancy/add_record.html',
                          {'form':form,'event':'Pregnancy'},
                          context_instance=RequestContext(request))
@login_required
def add_parturition(request,pregnancy_id):
	form = ParturitionForm()
	pregnancy = Pregnancy.objects.get(pk=pregnancy_id)
	if request.method == 'POST':
        	form = ParturitionForm(data=request.POST)
        	if form.is_valid():

			heats = Heat.objects.filter(pregnancy=pregnancy)
			for heat in heats:
				heat.completed = True
				heat.save()
			parts = Parturition.objects.filter(pregnancy=pregnancy)
			for part in parts:
				part.completed = True
				part.save()
			cons = Conceieved.objects.filter(pregnancy=pregnancy)
			for con in cons:
				con.completed = True
				con.save()
			pregnants = Pregnant.objects.filter(pregnancy=pregnancy)
			for pre in pregnants:
				pre.completed = True
				pre.save()


  			task = form.save(commit=False)
			task.added_on = datetime.now()
			task.pregnancy = pregnancy
			task.completed = False
			task.save()
			messages.add_message(request, messages.INFO, 'Pasturition record added successfully')
			url = "/pregnancy/record/%s/" % (pregnancy.id)
			return HttpResponseRedirect(url)

	return render_to_response('pregnancy/add_record.html',
                          {'form':form,'event':'Parturition'},
                          context_instance=RequestContext(request))



@login_required
def add_heat(request,pregnancy_id):
	form = HeatForm()
	pregnancy = Pregnancy.objects.get(pk=pregnancy_id)
	if request.method == 'POST':
        	form = HeatForm(data=request.POST)
        	if form.is_valid():
			heats = Heat.objects.filter(pregnancy=pregnancy)
			for heat in heats:
				heat.completed = True
				heat.save()
			parts = Parturition.objects.filter(pregnancy=pregnancy)
			for part in parts:
				part.completed = True
				part.save()
			cons = Conceieved.objects.filter(pregnancy=pregnancy)
			for con in cons:
				con.completed = True
				con.save()
			pregnants = Pregnant.objects.filter(pregnancy=pregnancy)
			for pre in pregnants:
				pre.completed = True
				pre.save()


			task = form.save(commit=False)
			task.added_on = datetime.now()
			task.pregnancy = pregnancy
			task.completed = False
			task.save()
			messages.add_message(request, messages.INFO, 'Heat record added successfully')
			url = "/pregnancy/record/%s/" % (pregnancy.id)
			return HttpResponseRedirect(url)


	return render_to_response('pregnancy/add_record.html',
                          {'form':form,'event':'Heat'},
                          context_instance=RequestContext(request))


@login_required
def add_conceieved(request,pregnancy_id):
	form = ConceievedForm()
	pregnancy = Pregnancy.objects.get(pk=pregnancy_id)
	
	if request.method == 'POST':
        	form = ConceievedForm(data=request.POST)
		heats = Heat.objects.filter(pregnancy=pregnancy)
		for heat in heats:
			heat.completed = True
			heat.save()
		parts = Parturition.objects.filter(pregnancy=pregnancy)
		for part in parts:
			part.completed = True
			part.save()
		cons = Conceieved.objects.filter(pregnancy=pregnancy)
		for con in cons:
			con.completed = True
			con.save()
		pregnants = Pregnant.objects.filter(pregnancy=pregnancy)
		for pre in pregnants:
			pre.completed = True
			pre.save()





        	if form.is_valid():
  			task = form.save(commit=False)
			task.added_on = datetime.now()
			task.pregnancy = pregnancy
			task.completed = False
			task.save()
			messages.add_message(request, messages.INFO, 'Conceieved record added successfully')
			url = "/pregnancy/record/%s/" % (pregnancy.id)
			return HttpResponseRedirect(url)

	return render_to_response('pregnancy/add_record.html',
                          {'form':form,'event':'Conceieved'},
                          context_instance=RequestContext(request))


@login_required
def add_pregnant(request,pregnancy_id):
	form = PregnantForm()
	pregnancy = Pregnancy.objects.get(pk=pregnancy_id)
	if request.method == 'POST':
        	form = PregnantForm(data=request.POST)
        	if form.is_valid():

			heats = Heat.objects.filter(pregnancy=pregnancy)
			for heat in heats:
				heat.completed = True
				heat.save()
			parts = Parturition.objects.filter(pregnancy=pregnancy)
			for part in parts:
				part.completed = True
				part.save()
			cons = Conceieved.objects.filter(pregnancy=pregnancy)
			for con in cons:
				con.completed = True
				con.save()
			pregnants = Pregnant.objects.filter(pregnancy=pregnancy)
			for pre in pregnants:
				pre.completed = True
				pre.save()


  			task = form.save(commit=False)
			task.added_on = datetime.now()
			task.pregnancy = pregnancy
			task.completed = False
			task.save()
			messages.add_message(request, messages.INFO, 'Pregnant record added successfully')
			url = "/pregnancy/record/%s/" % (pregnancy.id)
			return HttpResponseRedirect(url)


	return render_to_response('pregnancy/add_record.html',
                          {'form':form,'event':'Pregnant'},
                          context_instance=RequestContext(request))




@login_required
def add_treatment(request,pregnancy_id):
	form = TreatmentForm()
	pregnancy = Pregnancy.objects.get(pk=pregnancy_id)
	if request.method == 'POST':
        	form = TreatmentForm(data=request.POST)
        	if form.is_valid():
  			task = form.save(commit=False)
			task.added_on = datetime.now()
			task.pregnancy = pregnancy
			task.completed = False
			task.save()
			messages.add_message(request, messages.INFO, 'Treatment record added successfully')
			url = "/pregnancy/record/%s/" % (pregnancy.id)
			return HttpResponseRedirect(url)


	return render_to_response('pregnancy/add_record.html',
                          {'form':form,'event':'Treatment'},
                          context_instance=RequestContext(request))

@login_required
def add_result(request,stage,stage_id):
	if stage == "Heat":
	        stage_instance = Heat.objects.get(pk=stage_id)	
		form = HeatResultForm(instance=stage_instance)
	if stage == "Pregnant":
		stage_instance = Pregnancy.objects.get(pk=stage_id)
		form = PregnantResultForm(instance=stage_instance)
	if stage == "Parturition":
		stage_instance = Parturition.objects.get(pk=stage_id)
		form = ParturitionResultForm(instance=stage_instance)	
	if stage == "Coneieved":
		stage_instance = Conceieved.objects.get(pk=stage_id)
		form = ConeievedResultForm(instance=stage_instance)
	if stage == "Treatment":
		stage_instance = Treatment.objects.get(pk=stage_id)
		form = TreatmentResultForm(instance=stage_instance)


	if request.method == 'POST':
		if stage == "Heat":
			stage_instance = Heat.objects.get(pk=stage_id)
	
			form = HeatResultForm(data=request.POST,instance=stage_instance)
		if stage == "Pregnant":
			stage_instance = Pregnant.objects.get(pk=stage_id)

			form = PregnantResultForm(data=request.POST,instance=stage_instance)
		if stage == "Parturition":
			stage_instance = Parturition.objects.get(pk=stage_id)

			form = ParturitionResultForm(data=request.POST,instance=stage_instance)	
		if stage == "Coneieved":
			stage_instance = Conceieved.objects.get(pk=stage_id)
			form = ConeievedResultForm(data=request.POST,instance=stage_instance)
		if stage == "Treatment":
			stage_instance = Treatment.objects.get(pk=stage_id)
			form = TreatmentResultForm(data=request.POST,instance=stage_instance)


        	if form.is_valid():
			form.save()
			messages.add_message(request, messages.INFO, 'Record updated successfully')
			url = "/" 
			return HttpResponseRedirect(url)


	return render_to_response('pregnancy/add_result.html',
                          {'form':form,'event':stage,'instance':stage_instance},
                          context_instance=RequestContext(request))


























