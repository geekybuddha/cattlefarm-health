from django.db.models.signals import post_save
from dairy.cow.tasks import send_reminder

from datetime import datetime,timedelta

def reminder_handler(sender,instance, **kwargs):
	tomorrow = datetime.now() + timedelta(seconds=40)
	send_reminder.apply_async(eta=tomorrow, kwargs={'pk': "wwq"})

post_save.connect(reminder_handler, sender=HealthTask) 	

