from django.shortcuts import render_to_response
from cow.models import HealthTask,CowProfile,Dairy,Notifications,Status,UserDairy, Milk, UserProfile
from pregnancy.models import Pregnancy
from datetime import datetime
from django.http import HttpResponse,HttpResponseRedirect
from django.template import RequestContext, loader
from cow.forms import * 
from datetime import datetime
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from django.http import Http404
from pregnancy.models import *
from django.db.models import Q
from operator import attrgetter
from itertools import chain
from vaccination.models import Schedule
from alerts.models import MobileNumber
from django.shortcuts import get_object_or_404

from datetime import timedelta
from django.db.models import Count, Min, Sum, Avg
def index(request):
	form = UserForm()
	if request.user.is_authenticated():
		now = datetime.now()
		month = now.strftime("%B")


		## 


		vaccination = Schedule.objects.filter(time_of_vaccination=month)
		try:
			dairy = UserDairy.objects.get(user=request.user).dairy
		except:
			messages.add_message(request, messages.INFO, 'Please complete dairy profile to contiune using your account')
			return HttpResponseRedirect("/dairy/")

		cows = CowProfile.objects.filter(dairy=dairy).count()
	
		if cows <= 10:
			cow_message = "You have added only %d animal records, add all your animals to get full benefit of cattlebook!" % cows
		else:
			cow_message = ''

		status_form = StatusForm(dairy=dairy)	
		todays_tasks = HealthTask.objects.filter(due_date=datetime.now(),task_completed=False,cow__dairy=dairy)
		upcoming_tasks = HealthTask.objects.filter(due_date__gt=datetime.now(),task_completed=False,cow__dairy=dairy).order_by('due_date')[:5]
		due_tasks = HealthTask.objects.filter(due_date__lt=datetime.now(),task_completed=False,cow__dairy=dairy).order_by('-due_date')
		total_cows = CowProfile.objects.filter(dairy=dairy).count()
		total_tasks = HealthTask.objects.filter(cow__dairy=dairy).count()
		heat_list = Heat.objects.filter(pregnancy__cow__dairy=dairy,completed=False,due_date=datetime.now())
		conceieved_list = Conceieved.objects.filter(pregnancy__cow__dairy=dairy,completed=False,due_date=datetime.now())
		parturition_list = Parturition.objects.filter(pregnancy__cow__dairy=dairy,completed=False,due_date=datetime.now())
		pregnant_list = Pregnant.objects.filter(pregnancy__cow__dairy=dairy,completed=False,due_date=datetime.now())
		treatment_list = Treatment.objects.filter(pregnancy__cow__dairy=dairy,completed=False,due_date=datetime.now())
	
		status_list = Status.objects.filter(dairy=dairy,due_date=datetime.now())
		result_list = sorted(chain(status_list,conceieved_list,parturition_list,pregnant_list,treatment_list,heat_list,todays_tasks),key=attrgetter('due_date'))
		try:
			pregnant = Pregnant.objects.filter(pregnancy__cow__dairy=dairy).filter(Q(due_date__gte=now.date())).count()
		except:
			pregnant = 0
		try:
			heat = Heat.objects.filter(pregnancy__cow__dairy=dairy).filter(Q(due_date__gte=now.date())).count()
		except:
			heat = 0
		try:
			conceieved = Conceieved.objects.filter(pregnancy__cow__dairy=dairy).filter(Q(due_date__gte=now.date())).count()
		except:
			conceieved = 0
		try:
			parturition = Parturition.objects.filter(pregnancy__cow__dairy=dairy).filter(Q(due_date__gte=now.date())).count()
		except:
			parturition = 0


		completed_tasks = HealthTask.objects.filter(cow__dairy=dairy,task_completed=True).count()
		return render_to_response('cow/index.html',
				  {'vaccination':vaccination,'status_form':status_form,'cow_count':total_cows,'total_tasks':total_tasks,'completed_tasks':completed_tasks,'todays_tasks':result_list,
					'upcoming_tasks':upcoming_tasks,'due_tasks':due_tasks,'dairy':dairy,'cow_message':cow_message},
				  context_instance=RequestContext(request))
	else:
		form = UserForm()

		if request.method == "POST":
			form = UserForm(request.POST)
			if form.is_valid():

				new_user=User.objects.create_user(form.cleaned_data['username'],
					  form.cleaned_data['email'],
					  form.cleaned_data['password1'])
				new_user.first_name = form.cleaned_data['first_name']
				new_user.last_name = form.cleaned_data['last_name']
				new_user.save()
				user_profile = UserProfile(mobile_number=form.cleaned_data['mobile_number'],address=form.cleaned_data['address'])
				user_profile.save()
				messages.add_message(request, messages.INFO, 'Account created successfully. Please login to continue!')
				return HttpResponseRedirect('/dairy/')
		
		return render_to_response('index.html',{'form':form},
				  context_instance=RequestContext(request))

def all_tasks(request):
	if request.user.is_authenticated():
		now = datetime.now()
		month = now.strftime("%B")
		dairy = UserDairy.objects.get(user=request.user).dairy
		todays_tasks = HealthTask.objects.filter(task_completed=False,cow__dairy=dairy)
		heat_list = Heat.objects.filter(pregnancy__cow__dairy=dairy,completed=False)
		conceieved_list = Conceieved.objects.filter(pregnancy__cow__dairy=dairy,completed=False)
		parturition_list = Parturition.objects.filter(pregnancy__cow__dairy=dairy,completed=False)
		pregnant_list = Pregnant.objects.filter(pregnancy__cow__dairy=dairy,completed=False)
		treatment_list = Treatment.objects.filter(pregnancy__cow__dairy=dairy,completed=False)
	
		result_list = sorted(chain(conceieved_list,parturition_list,pregnant_list,treatment_list,heat_list,todays_tasks),key=attrgetter('due_date'))


		return render_to_response('alerts/alerts.html',
				  {'result_list':result_list,
				'dairy':dairy},
				  context_instance=RequestContext(request))
	else:
		
		return render_to_response('index.html',{},
				  context_instance=RequestContext(request))
@login_required
def cows_list(request):
	user_dairy = UserDairy.objects.get(user=request.user).dairy
	cows = CowProfile.objects.filter(dairy=user_dairy)
	return render_to_response('cow/cows.html',
                          {'cows':cows},
                          context_instance=RequestContext(request))
@login_required
def cow_profile(request,cow_id):
	user_dairy = UserDairy.objects.get(user=request.user).dairy
	cow = get_object_or_404(CowProfile,pk=cow_id)
	tasks = HealthTask.objects.filter(cow=cow)
	if cow.dairy.user == request.user:
		heat_list = Heat.objects.filter(pregnancy__cow=cow)
		conceieved_list = Conceieved.objects.filter(pregnancy__cow = cow)
		parturition_list = Parturition.objects.filter(pregnancy__cow=cow)
		pregnant_list = Pregnant.objects.filter(pregnancy__cow = cow)
		result_list = sorted(chain(heat_list, conceieved_list, parturition_list,pregnant_list),key=attrgetter('date'))

		return render_to_response('cow/cow.html',
                          {'cow':cow,'tasks':tasks,'result_list':result_list},
                          context_instance=RequestContext(request))
	else:
		return Http404

@login_required
def delete_task(request,task_id):
	user_dairy = UserDairy.objects.get(user=request.user).dairy
	task = get_object_or_404(HealthTask,pk=task_id)
	if task.cow.dairy.user == request.user:
		HealthTask.objects.filter(id=task.id).delete()
		return HttpResponseRedirect('/tasks/')
	else:
		return Http404
@login_required
def task_delete_confirm(request,task_id):
	user_dairy = UserDairy.objects.get(user=request.user).dairy
	task = get_object_or_404(HealthTask,pk=task_id)
	if task.cow.dairy.user == request.user:
		return render_to_response('cow/task_confirm_delete.html',{'task':task},
                          context_instance=RequestContext(request))
	else:
		return Http404
	




@login_required
def delete_cow(request,cow_id):
	user_dairy = UserDairy.objects.get(user=request.user).dairy
	cow = get_object_or_404(CowProfile,pk=cow_id)
	if cow.dairy.user == request.user:
		CowProfile.objects.filter(id=cow.id).delete()
		return HttpResponseRedirect('/cows/')
	else:
		return Http404
@login_required
def delete_confirm(request,cow_id):
	user_dairy = UserDairy.objects.get(user=request.user).dairy
	cow = get_object_or_404(CowProfile,pk=cow_id)
	if cow.dairy.user == request.user:
		return render_to_response('cow/confirm_delete.html',{'cow':cow},
                          context_instance=RequestContext(request))
	else:
		return Http404
	

@login_required
def health_task(request,task_id):
	user_dairy = UserDairy.objects.get(user=request.user).dairy
	task = get_object_or_404(HealthTask,pk=task_id)
	if task.cow.dairy.user == request.user:
		return render_to_response('cow/health_task.html',
                          {'task':task},
                          context_instance=RequestContext(request))
	else:
		return Http404
	


@login_required
def tasks_list(request):
	user_dairy = UserDairy.objects.get(user=request.user).dairy
	tasks = HealthTask.objects.filter(cow__dairy=user_dairy).order_by('-due_date')
	return render_to_response('cow/tasks.html',
                          {'tasks':tasks},
                          context_instance=RequestContext(request))


@login_required
def add_cow(request):
	form = CowProfileForm()

	if request.method == 'POST':
        	form = CowProfileForm(request.POST, request.FILES)
        	if form.is_valid():
			
			user_dairy = UserDairy.objects.get(user=request.user).dairy
  			cow = form.save(commit=False)
			cow.dairy = user_dairy
			cow.save()
			messages.add_message(request, messages.INFO, 'Cattle record added successfully')
			return HttpResponseRedirect('/cows/')
	else:
		return render_to_response('cow/add_cow.html',
                          {'form':form},
                          context_instance=RequestContext(request))

@login_required
def add_health_task(request):
	form = HealthTaskForm()

	if request.method == 'POST':
        	form = HealthTaskForm(request.POST, request.FILES)
        	if form.is_valid():
  			task = form.save(commit=False)
			task.added_on = datetime.now()
			task.task_completed = False
			task.content_object = request.user
			task.save()
			messages.add_message(request, messages.INFO, 'Health task added successfully')
			return HttpResponseRedirect('/tasks/')

	else:
		return render_to_response('cow/add_health_task.html',
                          {'form':form},
                          context_instance=RequestContext(request))


@login_required
def add_next_health_task(request,task_id):
	form = HealthTaskForm()

	health_task = HealthTask.objects.get(pk=task_id)
	if request.method == 'POST':
        	form = HealthTaskForm(request.POST, request.FILES)
        	if form.is_valid():
  			task = form.save(commit=False)
			task.added_on = datetime.now()
			task.task_completed = False
			task.parent_task = health_task
			task.save()
			messages.add_message(request, messages.INFO, 'Health task added successfully')
			return HttpResponseRedirect('/tasks/')

	else:
		return render_to_response('cow/add_health_task.html',
                          {'form':form},
                          context_instance=RequestContext(request))

@login_required
def settings(request):
	try:
		notifications = Notifications.objects.get(user=request.user)
		form = NotificationsForm(instance=notifications)
	except Notifications.DoesNotExist:
		form = NotificationsForm()
	dairy = UserDairy.objects.get(user=request.user)

	numbers = MobileNumber.objects.filter(dairy=dairy)
	if request.method=='POST':
		try:
				
			notifications = Notifications.objects.get(user=request.user)
			form = NotificationsForm(request.POST,instance=notifications)
			form.save()
		except Notifications.DoesNotExist:
			form = NotificationsForm(request.POST)
			notification = form.save(commit=False)
			notification.user = request.user
			notification.save()
		messages.add_message(request, messages.INFO, 'Notifications settings updated')
	return render_to_response('settings.html',
                          {'form':form,'numbers':numbers},
                          context_instance=RequestContext(request))

@login_required
def dairy_profile(request):
	try:
		dairy = UserDairy.objects.get(user=request.user).dairy
		form = DairyForm(instance=dairy)
	except UserDairy.DoesNotExist:
		form = DairyForm()
	if request.method=='POST':
		try:
			dairy = UserDairy.objects.get(user=request.user).dairy
			form = DairyForm(request.POST,instance=dairy)
			form.save()
		except UserDairy.DoesNotExist:
			form = DairyForm(request.POST)
			dairy = form.save(commit=False)
			dairy.user = request.user
			dairy.save()

			user_dairy = UserDairy(dairy=dairy,user=request.user)
			user_dairy.save()

		messages.add_message(request, messages.INFO, 'Dairy profile updated')
	return render_to_response('cow/dairy.html',
                          {'form':form},
                          context_instance=RequestContext(request))

	
	
@login_required
def update_cow(request,cow_id):
	cow = CowProfile.objects.get(pk=cow_id)
	form = CowProfileForm(instance=cow)
	if request.method == 'POST':
        	form = CowProfileForm(request.POST, request.FILES,instance=cow)
        	if form.is_valid():
  			cow_form = form.save(commit=False)
			cow_form.save()
			messages.add_message(request, messages.INFO, 'Cattle details updated successfully')
			return HttpResponseRedirect('/cows/')

	return render_to_response('cow/add_cow.html',
                          {'form':form,'cow':cow},
                          context_instance=RequestContext(request))


	
	
@login_required
def update_health_task(request,task_id):
	health_task = HealthTask.objects.get(pk=task_id)
	form = HealthTaskCompletedForm()
	if request.method == 'POST':
        	form = HealthTaskCompletedForm(request.POST, request.FILES,instance=health_task)
        	if form.is_valid():
  			task = form.save(commit=False)
			task.completed_on = datetime.now()
			task.task_completed = True
			task.save()
			messages.add_message(request, messages.INFO, 'Health task updated successfully')
			return HttpResponseRedirect('/tasks/')

	return render_to_response('cow/update_health_task.html',
                          {'form':form,'task':health_task},
                          context_instance=RequestContext(request))



def homepage(request):
        return render_to_response('cow/homepage.html',
                          {},
                          context_instance=RequestContext(request))


@login_required
def post_status(request):
	if request.method == "POST":

		dairy = UserDairy.objects.get(user=request.user).dairy
		form = StatusForm(dairy=dairy,data=request.POST,files=request.FILES)
		if form.is_valid():
			status = form.save(commit=False)
			status.due_date = datetime.now()
			status.user = request.user
			status.dairy = dairy
			status.save()
			return HttpResponseRedirect("/")

@login_required
def add_milk_view(request):
	dairy = UserDairy.objects.get(user=request.user).dairy
	date = datetime.now().date
	cows = CowProfile.objects.filter(dairy=dairy)
	full_list =[]
	total = 0
	morning = 0
	evening = 0
	form = MilkSearchForm()
	if request.method == "POST":
		
		form = MilkSearchForm(data=request.POST)
		if form.is_valid():
			date = form.cleaned_data['date']
			milks = Milk.objects.filter(dairy=dairy,date=date)
	else:
		milks = Milk.objects.filter(dairy=dairy,date=date)
	for cow in cows:
		data = {}
		data['cow'] = cow
		for milk in milks:
			if milk.cow.id == cow.id:
				data['milk'] = milk
				data['total'] = milk.morning+milk.evening
				data['morning'] = milk.morning
				data['evening'] = milk.evening
				total = total+data['total']
				morning = morning+data['morning']
				evening = evening+data['evening']
		full_list.append(data)

	return render_to_response('cow/milk.html',
                          {'form':form,'full_list':full_list,'date':date,'total':total,'morning':morning,'evening':evening},
                          context_instance=RequestContext(request))

@login_required
def milk_report(request):
	dairy = UserDairy.objects.get(user=request.user).dairy
	date = datetime.now().date
	cows = CowProfile.objects.filter(dairy=dairy)
	full_list =[]
	total = 0
	morning = 0
	evening = 0
	form = MilkSearchForm()
	if request.method == "POST":
		
		form = MilkSearchForm(data=request.POST)
		if form.is_valid():
			date = form.cleaned_data['date']
			milks = Milk.objects.filter(dairy=dairy,date=date)
	else:
		milks = Milk.objects.filter(dairy=dairy,date=date)
	for cow in cows:
		data = {}
		data['cow'] = cow
		for milk in milks:
			if milk.cow.id == cow.id:
				data['milk'] = milk
				data['total'] = milk.morning+milk.evening
				data['morning'] = milk.morning
				data['evening'] = milk.evening
				total = total+data['total']
				morning = morning+data['morning']
				evening = evening+data['evening']
		full_list.append(data)

	return render_to_response('cow/milk-report.html',
                          {'form':form,'full_list':full_list,'date':date,'total':total,'morning':morning,'evening':evening},
                          context_instance=RequestContext(request))




@login_required
def milk(request):
	dairy = UserDairy.objects.get(user=request.user).dairy
	date = datetime.now().date
	today = Milk.objects.filter(cow__dairy=dairy,date=date)
	today_morning = today.aggregate(morning=Sum('morning'))['morning']
	today_evening = today.aggregate(evening=Sum('evening'))['evening']
	if not today_morning:
		today_morning = 0
	if not today_evening:
		today_evening = 0
	data = {}
	data['today_total'] = today_evening+today_morning


	end_date = datetime.now()- timedelta(days=7)
	todays_date = datetime.now()
	monthly = Milk.objects.filter(dairy=dairy,date__month=todays_date.month)


	report = []
	end_date = datetime.now()
	day = timedelta(days=1)
	month = timedelta(days=30)
	start_date = end_date- month
	while start_date <= end_date:
		data = {}
		data['date'] = start_date
		today = Milk.objects.filter(dairy=dairy,date=start_date)
		morning = today.aggregate(total=Sum('morning'))['total']
		if not morning:
			morning = 0
		evening = today.aggregate(total=Sum('evening'))['total']
		if not evening:
				evening = 0
		data['total'] = morning+evening
 		start_date = start_date+day
		report.append(data)
	form = MilkSearchForm()
	return render_to_response('cow/milk_dashboard.html',
                          {'data2':report,'form':form,'data':data,'date':date,},
                          context_instance=RequestContext(request))



import time
from time import mktime

@login_required
def add_milk(request):
	cow_id = request.GET['cow']
	dairy = get_object_or_404(UserDairy,user=request.user).dairy
	cow = get_object_or_404(CowProfile,dairy=dairy,pk=cow_id)
	#date = datetime.now()
	phase = request.GET['phase']
	quantity = request.GET['quantity']
	date = request.GET['date']
	try:
		milk = Milk.objects.get(dairy=dairy,cow=cow,date=date)
		if phase == "morning":
			milk.morning = quantity
			milk.save()
		if phase == "evening":
			milk.evening = quantity
			milk.save()	
		return HttpResponse("milk record updated")
	except Milk.DoesNotExist:
		if phase == "morning":
			milk = Milk(dairy=dairy,cow=cow,date=date,morning=quantity,evening=0.0)
			milk.save()
		if phase == "evening":
			milk = Milk(dairy=dairy,cow=cow,date=date,evening=quantity,morning=0.0)
			milk.save()
		return HttpResponse("milk record added")


def zoho(request):
	return HttpResponse("1428464073458")
def signup(request):
	form = UserForm()

	if request.method == "POST":
		form = UserForm(request.POST)
		if form.is_valid():

			new_user=User.objects.create_user(form.cleaned_data['username'],
                                  form.cleaned_data['email'],
                                  form.cleaned_data['password1'])
			new_user.first_name = form.cleaned_data['first_name']
			new_user.last_name = form.cleaned_data['last_name']
			new_user.save()
			user_profile = UserProfile(mobile_number=form.cleaned_data['mobile_number'],address=form.cleaned_data['address'])
			user_profile.save()
			messages.add_message(request, messages.INFO, 'Account created successfully. Please login to continue')
			return HttpResponseRedirect('/dairy/')
			
	return render_to_response('signup.html',
                          {'form':form},
                          context_instance=RequestContext(request))


