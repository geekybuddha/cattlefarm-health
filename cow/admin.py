from django.contrib import admin

from cow.models import *

admin.site.register(CowProfile)
admin.site.register(HealthTask)
admin.site.register(Notifications)
admin.site.register(Dairy)
admin.site.register(UserDairy)
admin.site.register(Status)
admin.site.register(Milk)
