# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'HealthTask.task_name'
        db.add_column(u'cow_healthtask', 'task_name',
                      self.gf('django.db.models.fields.CharField')(default='something', max_length=100),
                      keep_default=False)

        # Adding field 'HealthTask.parent_task'
        db.add_column(u'cow_healthtask', 'parent_task',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cow.HealthTask'], null=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'HealthTask.task_name'
        db.delete_column(u'cow_healthtask', 'task_name')

        # Deleting field 'HealthTask.parent_task'
        db.delete_column(u'cow_healthtask', 'parent_task_id')


    models = {
        'cow.cowprofile': {
            'Meta': {'object_name': 'CowProfile'},
            'breed': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'date_of_birth': ('django.db.models.fields.DateField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'})
        },
        'cow.healthtask': {
            'Meta': {'object_name': 'HealthTask'},
            'added_on': ('django.db.models.fields.DateField', [], {}),
            'completed_on': ('django.db.models.fields.DateField', [], {}),
            'cow': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cow.CowProfile']"}),
            'due_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'parent_task': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cow.HealthTask']", 'null': 'True'}),
            'result': ('django.db.models.fields.TextField', [], {}),
            'task_completed': ('django.db.models.fields.BooleanField', [], {}),
            'task_details': ('django.db.models.fields.TextField', [], {}),
            'task_name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['cow']