# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'HealthTask'
        db.create_table(u'cow_healthtask', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cow', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cow.CowProfile'])),
            ('task_details', self.gf('django.db.models.fields.TextField')()),
            ('added_on', self.gf('django.db.models.fields.DateField')()),
            ('due_date', self.gf('django.db.models.fields.DateField')()),
            ('result', self.gf('django.db.models.fields.TextField')()),
            ('task_completed', self.gf('django.db.models.fields.BooleanField')()),
        ))
        db.send_create_signal(u'cow', ['HealthTask'])


    def backwards(self, orm):
        # Deleting model 'HealthTask'
        db.delete_table(u'cow_healthtask')


    models = {
        u'cow.cowprofile': {
            'Meta': {'object_name': 'CowProfile'},
            'breed': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'date_of_birth': ('django.db.models.fields.DateField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'})
        },
        u'cow.healthtask': {
            'Meta': {'object_name': 'HealthTask'},
            'added_on': ('django.db.models.fields.DateField', [], {}),
            'cow': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cow.CowProfile']"}),
            'due_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'result': ('django.db.models.fields.TextField', [], {}),
            'task_completed': ('django.db.models.fields.BooleanField', [], {}),
            'task_details': ('django.db.models.fields.TextField', [], {})
        }
    }

    complete_apps = ['cow']