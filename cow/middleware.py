from cow.models import CowProfile,UserDairy
from pregnancy.models import *
from datetime import datetime
from django.db.models import Q

from vaccination.models import Schedule

def home_context_processor(request):
	if request.user.is_authenticated():
		try:
			dairy = UserDairy.objects.get(user=request.user).dairy

			now = datetime.now()
			total_cows = CowProfile.objects.filter(dairy=dairy).count()
			try:
				pregnant = Pregnant.objects.filter(pregnancy__cow__dairy=dairy,completed=False).count()
			except:
				pregnant = 0
			try:
				heat = Heat.objects.filter(pregnancy__cow__dairy=dairy,completed=False).count()
			except:
				heat = 0
			try:
				conceieved = Conceieved.objects.filter(pregnancy__cow__dairy=dairy,completed=False).count()
			except:
				conceieved = 0
			try:
				parturition = Parturition.objects.filter(pregnancy__cow__dairy=dairy,completed=False).count()
			except:
				parturition = 0

			month = now.strftime("%B")
			vaccination = Schedule.objects.filter(time_of_vaccination=month)

			return  {'vaccination':vaccination,'cow_count':total_cows,'parturition':parturition,'pregnant':pregnant,'heat':heat, 'conceieved':conceieved,'dairy':dairy}
		except:
			return {}
	else:
		return {}
