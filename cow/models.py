from django.db import models
from django.contrib.contenttypes import generic
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from django.utils.translation import ugettext_lazy as _

class Dairy(models.Model):
	name = models.CharField(_('name'),max_length=100)
	address = models.TextField(_('address'))
	user = models.ForeignKey(User)
	class Meta:
                app_label = 'cow'
		verbose_name = _('dairy')
        def __unicode__(self):
                return '%s' % (self.name)


class UserProfile(models.Model):
	mobile_number = models.CharField(_('name'),max_length=100)
	address = models.CharField(max_length=255)
	
class CowProfile(models.Model):
	name = models.CharField(_('name'),max_length=100)
	date_of_birth = models.DateField(_('date of birth'))
	description = models.TextField(_('description'))
	photo = models.ImageField(_('photo'),upload_to='cows',null=True,blank=True)
	breed = models.CharField(_('breed'),max_length=100)
        tasks = generic.GenericRelation('HealthTask')
	dairy = models.ForeignKey(Dairy)

	class Meta:
	        app_label = 'cow'
		verbose_name = _('cattle')
        def __unicode__(self):
                return '%s' % (self.name)

class HealthTask(models.Model):
	cow = models.ForeignKey(CowProfile)
	task_name = models.CharField(_('task name'),max_length=100)
	task_details = models.TextField(_('task details'))
	added_on = models.DateField(_('added on'))
	due_date = models.DateField(_('due date'))
	completed_on = models.DateField(_('completed on'),null=True)
	result = models.TextField(_('result'),null=True)
	parent_task = models.ForeignKey('self',null= True)
	task_completed = models.BooleanField(_('task completed'))

	content_type = models.ForeignKey(ContentType)
    	object_id = models.PositiveIntegerField()
    	content_object = generic.GenericForeignKey('content_type', 'object_id')

	class Meta:
                app_label = 'cow'
		verbose_name = _('health task')

        def __unicode__(self):
                return '%s' % (self.task_name)

class Notifications(models.Model):
	email_notifications = models.BooleanField(_('email notifications'))
	sms_notifications = models.BooleanField(_('sms notifications'))
	mobile_number = models.IntegerField(_('mobile number'))
	user = models.OneToOneField(User)

class Status(models.Model):
	cow = models.ForeignKey(CowProfile,null=True,blank=True)
	task_name = models.TextField(_('task name'))
	image = models.ImageField(_('image'),upload_to='media',null=True,blank=True)
	due_date = models.DateField(_('due date'))
	user = models.ForeignKey(User)	
	dairy = models.ForeignKey(Dairy)
	class Meta:
                app_label = 'cow'
		verbose_name = _('status')

        def __unicode__(self):
                return '%s' % (self.task_name)


class UserDairy(models.Model):
	user = models.ForeignKey(User)
	dairy = models.ForeignKey(Dairy)
	class Meta:
		verbose_name = _('dairy')

class Milk(models.Model):
	dairy = models.ForeignKey(Dairy)
	morning = models.FloatField(_('morning'))
	evening = models.FloatField(_('evening'))
	date = models.DateField(_('date'))
	cow = models.ForeignKey(CowProfile)
	
	class Meta:
		verbose_name = _('milk')
import urllib
import httplib

def send_sms(mobilenumber,text):
	conn = httplib.HTTPConnection("smslane.com")
	mobile_number = '91'+str(mobilenumber)
	params = urllib.urlencode({'user': 'skbohra123', 'password': '461354','msisdn':mobile_number,'sid':'WebSMS','msg':text,'fl':'0'})
	url = "http://smslane.com/vendorsms/pushsms.aspx?%s" % params
	conn.request("GET", url)
	r1 = conn.getresponse()
	print "data -"
	print r1.read()
	conn.close()
	



from celery.decorators import task
from cow.models import HealthTask
from django.core.mail import send_mail

from django.core.mail import EmailMultiAlternatives

@task
def send_reminder(pk):
    task = HealthTask.objects.get(id=pk)
    if task.cow.dairy.user.notifications.email_notifications == True:
    	html_body = "Hi <br><br> Your cow %s needs to be checked for the task - %s today! This is a reminder for the same! <br><br> Cheers<br> DairyHealth" % (task.cow, task.task_name)	
    	body = "Hi, Your cow %s needs to be checked for the task - %s today! This is a reminder for the same! Cheers" % (task.cow, task.task_name)	
    	to = [task.cow.dairy.user.email,"shreekant@ringad.net",]
    	subject = "DairyHealth - Health Task for today reminder"
    	msg = EmailMultiAlternatives(subject, body, "info@dairyhealth.cu.cc", to)
    	msg.attach_alternative(html_body, "text/html") 
    	msg.send()
    if task.cow.dairy.user.notifications.sms_notifications == True:
	mobile = task.cow.dairy.user.notifications.mobile_number
    	body = "Hi, Your cow %s needs to be checked for the task - %s today! This is a reminder for the same! Cheers" % (task.cow, task.task_name)        
        send_sms(mobile,body)	


from django.db.models.signals import post_save

from datetime import time
from datetime import datetime
from datetime import timedelta
import pytz

def reminder_handler(sender,instance, **kwargs):
	#tomorrow = datetime.datetime.now()+datetime.timedelta(seconds=30)
	indian = pytz.timezone("Asia/Kolkata")
	tomorrow = datetime.combine(instance.due_date, time(12,05,10))
	date = indian.localize(tomorrow)
	send_reminder.apply_async(eta=date, kwargs={'pk': instance.id})

#post_save.connect(reminder_handler, sender=HealthTask) 	

