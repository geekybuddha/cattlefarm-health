from django.forms import ModelForm
from django import forms
from cow.models import Dairy,CowProfile,HealthTask,Notifications,Status
from django.contrib.auth.models import User

class DairyForm(ModelForm):
	class Meta:
		model = Dairy
		exclude = ('user',)

class NotificationsForm(ModelForm):
	class Meta:
		model = Notifications
		exclude = ('user',)
class CowProfileForm(ModelForm):
	class Meta:
		model = CowProfile
		exclude = ('dairy',)
class HealthTaskForm(ModelForm):
	class Meta:
		model = HealthTask
		exclude = ('content_type','object_id','completed_on','result','parent_task','added_on','task_completed',)


class HealthTaskCompletedForm(ModelForm):
	class Meta:
		model = HealthTask
		fields = ('result',)

class StatusForm(ModelForm):
	class Meta:
		model = Status
		exclude = ('due_date','user','dairy',)
	def __init__(self, dairy, *args, **kwargs):
        	super(StatusForm, self).__init__(*args, **kwargs)
        	self.fields['cow'].queryset=CowProfile.objects.filter(dairy=dairy)


class MilkSearchForm(forms.Form):
	date = forms.DateField()



class UserForm(forms.Form):
	username = forms.CharField(max_length=30)
	first_name = forms.CharField()
	last_name = forms.CharField()
	password1=forms.CharField(max_length=30,widget=forms.PasswordInput(),label="Password") #render_value=False
	password2=forms.CharField(max_length=30,widget=forms.PasswordInput(),label="Repeat Password")
	email=forms.EmailField(required=False)
        mobile_number = forms.IntegerField()	
	address = forms.CharField()
	def clean_username(self): # check if username dos not exist before
    		try:
        		User.objects.get(username=self.cleaned_data['username']) #get user from user model
    		except User.DoesNotExist :
        		return self.cleaned_data['username']

    		raise forms.ValidationError("this user exist already")


	def clean(self): # check if password 1 and password2 match each other
    		if 'password1' in self.cleaned_data and 'password2' in self.cleaned_data:#check if both pass first validation
        		if self.cleaned_data['password1'] != self.cleaned_data['password2']: # check if they match each other
            			raise forms.ValidationError("passwords dont match each other")
    		return self.cleaned_data


	def clean_mobile_number(self): # check if scholar_id dos not exist before
    		data = self.cleaned_data['mobile_number']
        	length = len(str(data))
		if length ==10:
			return self.cleaned_data['mobile_number']

    		raise forms.ValidationError("Mobile number should be of 10 digits")

