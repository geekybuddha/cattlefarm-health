"""Django settings for dairy project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""
from datetime import timedelta

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
SITE_ID = 1

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'pbyce^p*93z6)c-_8vaqt&tzk56moy7ue(22uy4eo-wc^j$=wn'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

TEMPLATE_DEBUG = True


TEMPLATE_CONTEXT_PROCESSORS = (
	'django.contrib.auth.context_processors.auth',
	'django.core.context_processors.i18n',
	'cow.middleware.home_context_processor',
        'django.contrib.messages.context_processors.messages',
)

ALLOWED_HOSTS = ['cattlebook.co']

ADMINS = (('John', 'skbohra123@gmail.com'))
 # Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.humanize',
    'south',
    'bootstrap3',
    'kombu.transport.django',
    'django_comments',
    'django_weather_widget',
    'cow',
    'pregnancy',
    'vaccination',
    'alerts',
    'tracking',
    'paypal',
)
MIDDLEWARE_CLASSES = (
    'tracking.middleware.VisitorTrackingMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'dairy.urls'

WSGI_APPLICATION = 'dairy.wsgi.application'

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),

    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Asia/Kolkata'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = 'skbohra123@gmail.com'
SERVER_EMAIL = 'skbohra123@gmail.com'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'skbohra123@gmail.com'
EMAIL_HOST_PASSWORD = 'amazinglife90'

STATIC_URL = '/static/'
TEMPLATE_DIRS = ('/home/skbohra/code/dairy/templates/', )
#STATIC_ROOT = "/home/skbohra/code/dairy/static/"
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
    '/home/skbohra/code/dairy/static/',
)
MEDIA_ROOT = '/home/skbohra/code/dairy/media/'
MEDIA_URL = "/media/"

LOGIN_REDIRECT_URL = '/'


CELERYBEAT_SCHEDULE = {
    'add-every-30-seconds': {
        'task': 'tasks.add',
        'schedule': timedelta(seconds=30),
        'args': (16, 16)
    },
}

CELERY_TIMEZONE = 'UTC'

TRACK_PAGEVIEWS = True
TRACK_REFERER = True

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    },
    'userlog': {
        'BACKEND': 'redis_cache.RedisCache',
        'LOCATION': 'localhost:6379',
        'TIMEOUT': 3600,
        'KEY_PREFIX': 'userlog',
    },
}

try:
   from local_settings import *
except ImportError, e:
   pass


SUBSCRIPTION_PAYPAL_SETTINGS = {
    "cmd": "_xclick-subscriptions",
    "business": "shreekantbohra@gmail.com",
}

PAYPAL_RECEIVER_EMAIL = "shreekantbohra@gmail.com"
