"""
WSGI config for dairy project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/howto/deployment/wsgi/
"""

#import os
#os.environ.setdefault("DJANGO_SETTINGS_MODULE", "dairy.settings")

#from django.core.wsgi import get_wsgi_application
#application = get_wsgi_application()


import os,sys

sys.path.append('/home')
sys.path.append('/home/skbohra')
sys.path.append('/home/skbohra/code')
sys.path.append('/home/skbohra/code/dairy')
sys.path.append('/home/skbohra/code/dairy/dairy')
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()


