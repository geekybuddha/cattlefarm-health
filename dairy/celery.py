from __future__ import absolute_import

import os

from celery import Celery

from django.conf import settings

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'dairy.settings')
app = Celery('dairy')

# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

app.conf.update(
    CELERY_IMPORTS = ("tasks", ) 
)
app.conf.update(
    CELERY_TIMEZONE='Asia/Kolkata',
    CELERY_ENABLE_UTC=True,
    BROKER_URL = 'redis://localhost:6379/0'
)
from celery.schedules import crontab

app.conf.update(
CELERYBEAT_SCHEDULE = {
    'todys-task-reminder-every-minute': {
        'task': 'tasks.todays_tasks_reminder',
        'schedule': crontab(minute=0,hour=10),
    },
    'pending-tasks-reminder-every-minute': {
        'task': 'tasks.pending_tasks_reminder',
        'schedule': crontab(minute=10,hour=10),
    },
    'critical-alerts-every-minute': {
        'task': 'tasks.send_alerts',
        'schedule': crontab(minute=50,hour=9),
    },

}
)



