from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',

    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^$', 'cow.views.index', name='home'),
    url(r'^all_tasks/$', 'cow.views.all_tasks', name='all_tasks'),
    url(r'^add_cow/', 'cow.views.add_cow', name='add_cow'),
    url(r'^cows/', 'cow.views.cows_list', name='cows'),
    url(r'^tasks/', 'cow.views.tasks_list', name='tasks'),
    url(r'^add_health_task/', 'cow.views.add_health_task', name='add_health_task'),
    url(r'^settings/', 'cow.views.settings', name='settings'),
    url(r'^dairy/', 'cow.views.dairy_profile', name='dairy'),
    url(r'^home/', 'cow.views.homepage', name='homepage'),
    url(r'^post_status/', 'cow.views.post_status', name='post_status'),
    url(r'^task/(?P<task_id>\d+)/$', 'cow.views.health_task', name='health_task'),
    url(r'^cow/(?P<cow_id>\d+)/$', 'cow.views.cow_profile', name='cow_profile'),
    url(r'^update_cow/(?P<cow_id>\d+)/$', 'cow.views.update_cow', name='update_cow'),
    url(r'^cow/delete/confirm/(?P<cow_id>\d+)/$', 'cow.views.delete_confirm', name='delete_confirm'),
    url(r'^cow/delete/(?P<cow_id>\d+)/$', 'cow.views.delete_cow', name='delete_cow'),
    url(r'^task/delete/confirm/(?P<task_id>\d+)/$', 'cow.views.task_delete_confirm', name='task_delete_confirm'),
    url(r'^task/delete/(?P<task_id>\d+)/$', 'cow.views.delete_task', name='delete_task'),

    #pregnancy module 

    
    url(r'^pregnancy/$', 'pregnancy.views.index', name='pregnancy_index'),
    url(r'^pregnancy/add_record/$', 'pregnancy.views.add_record', name='pregnancy_add_record'),

    url(r'^pregnancy/record/(?P<pregnancy_id>\d+)/$', 'pregnancy.views.view_record', name='view_record'),
    url(r'^pregnancy/add_heat/(?P<pregnancy_id>\d+)/$', 'pregnancy.views.add_heat', name='add_heat'),
    url(r'^pregnancy/add_parturition/(?P<pregnancy_id>\d+)/$', 'pregnancy.views.add_parturition', name='add_pasturition'),
    url(r'^pregnancy/add_conceieved/(?P<pregnancy_id>\d+)/$', 'pregnancy.views.add_conceieved', name='add_conceieved'),
    url(r'^pregnancy/add_pregnant/(?P<pregnancy_id>\d+)/$', 'pregnancy.views.add_pregnant', name='add_pregnant'),
    url(r'^pregnancy/add_treatment/(?P<pregnancy_id>\d+)/$', 'pregnancy.views.add_treatment', name='add_treatment'),
    url(r'^pregnancy/result/(?P<stage>\w+)/(?P<stage_id>\d+)/$', 'pregnancy.views.add_result', name='add_treatment'),


    url(r'^alerts/$', 'alerts.views.index', name='alerts'),
    url(r'^alerts/add_number/$', 'alerts.views.add_mobile_number', name='add_number'),
    url(r'^status/$', 'alerts.views.cow_status', name='cow_status'),
    
    
    url(r'^milk/$', 'cow.views.milk', name='cow_milk'),
    url(r'^milk/report/$', 'cow.views.milk_report', name='cow_milk_report'),
    url(r'^add_milk/$', 'cow.views.add_milk_view', name='add_cow_milk'),
    url(r'^milk/add/$', 'cow.views.add_milk', name='add_cow_milk'),
    url(r'^zohoverify/verifyforzoho.html/$', 'cow.views.zoho', name='zoho'),
    url(r'^signup/$', 'cow.views.signup', name='signup'),




    url(r'^comments/', include('django_comments.urls')),


    url(r'^update_task/(?P<task_id>\d+)/$', 'cow.views.update_health_task', name='update_health_task'),
    url(r'^add_next_task/(?P<task_id>\d+)/$', 'cow.views.add_next_health_task', name='next_health_task'),
    # url(r'^blog/', include('blog.urls')),
    (r'^accounts/login/$', 'django.contrib.auth.views.login', {'template_name': 'login.html'}),
    (r'^accounts/logout/$', 'django.contrib.auth.views.logout', {'next_page':'/accounts/login/','template_name': 'login.html'}),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^tracking/', include('tracking.urls')),
)



from django.conf import settings
from django.conf.urls.static import static

urlpatterns =urlpatterns + patterns('',
    # ... the rest of your URLconf goes here ...
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


from django.conf import settings

if 'rosetta' in settings.INSTALLED_APPS:
    urlpatterns += patterns('',
        url(r'^rosetta/', include('rosetta.urls')),
    )
