
from django.forms import ModelForm
from alerts.models import MobileNumber

class MobileNumberForm(ModelForm):
	class Meta:
		model = MobileNumber
		exclude = ('dairy',)


