from django.contrib import admin

from alerts.models import *

admin.site.register(Alert)
admin.site.register(SentAlert)
