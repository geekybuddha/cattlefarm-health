from django.db import models
from cow.models import CowProfile,Dairy
from django.utils.translation import ugettext_lazy as _

class Alert(models.Model):
	OPTIONS = (
	('heat','Heat'),
	('pregnant','Preganant'),
	('parturition','Partution'),
	('conceived','Conceived'),
	)
	stage = models.CharField(_('stage'),choices=OPTIONS,max_length=50) 
 	day = models.IntegerField(_('day'))
	message = models.TextField(_('message'))


class SentAlert(models.Model):
	cow = models.ForeignKey(CowProfile)
	alert = models.ForeignKey(Alert)

class MobileNumber(models.Model):
	number = models.IntegerField(_('number'))
	dairy = models.ForeignKey(Dairy)
	is_subscribed = models.BooleanField(_('is subscribed'))
