from django.shortcuts import render
from cow.models import *
from pregnancy.models import *
from alerts.models import *
from datetime import datetime 
import time 
from django.http import HttpResponse
from alerts.forms import MobileNumberForm
from django.template import RequestContext, loader
from django.shortcuts import render_to_response

from datetime import timedelta
from operator import attrgetter
from itertools import chain
from django.http import HttpResponse,HttpResponseRedirect

def index(request):
	try:
		now = request.GET['date']
		now = time.strptime(now,"%B %d, %Y")
		now = datetime.fromtimestamp(time.mktime(now)).date()
	except:
		now = datetime.date(datetime.now())
	day = timedelta(days=1)
	previous_date = now - day
	next_date = now + day
	dairy = UserDairy.objects.get(user=request.user).dairy
	cows = CowProfile.objects.filter(dairy=dairy)


	notifications = []
	'''
	heat alerts
	'''
	cows = Heat.objects.filter(pregnancy__cow__dairy=dairy,completed=False)
	alerts = Alert.objects.filter(stage="heat")
	for heat in cows:
		days = now - heat.date

		for alert in alerts:
			if days.days == alert.day:
				data = {}
				data['alert'] = alert
				data['cow'] = heat.pregnancy.cow
				data['stage'] = heat
				notifications.append(data)
				
	'''
	partution alerts
	'''
	cows = Parturition.objects.filter(pregnancy__cow__dairy=dairy,completed=False)
	alerts = Alert.objects.filter(stage="parturition")
	for heat in cows:
		days = now - heat.date	
		for alert in alerts:
			if days.days == alert.day:
				data = {}
				data['alert'] = alert
				data['cow'] = heat.pregnancy.cow
				data['stage'] = heat
				notifications.append(data)


	'''
	conceivevd alerts
	'''
	cows = Conceieved.objects.filter(pregnancy__cow__dairy=dairy,completed=False)
	alerts = Alert.objects.filter(stage="conceived")
	for heat in cows:
		days = now - heat.date
		for alert in alerts:
			if days.days == alert.day:
				data = {}
				data['alert'] = alert
				data['cow'] = heat.pregnancy.cow
				data['stage'] = heat
				notifications.append(data)

	'''
	pregnant alerts
	'''
	cows = Pregnant.objects.filter(pregnancy__cow__dairy=dairy,completed=False)
	alerts = Alert.objects.filter(stage="pregnant")
	for heat in cows:
		days = now - heat.date	
		for alert in alerts:
			if days.days == alert.day:
				data = {}
				data['alert'] = alert
				data['cow'] = heat.pregnancy.cow
				data['stage'] = heat
				notifications.append(data)

	month = now.strftime("%B")
	todays_tasks = HealthTask.objects.filter(due_date=now,task_completed=False,cow__dairy=dairy)
	upcoming_tasks = HealthTask.objects.filter(due_date__gt=now,task_completed=False,cow__dairy=dairy).order_by('due_date')
	due_tasks = HealthTask.objects.filter(due_date__lt=now,task_completed=False,cow__dairy=dairy).order_by('-due_date')
	heat_list = Heat.objects.filter(pregnancy__cow__dairy=dairy,completed=False,due_date=now)
	conceieved_list = Conceieved.objects.filter(pregnancy__cow__dairy=dairy,completed=False,due_date=now)
	parturition_list = Parturition.objects.filter(pregnancy__cow__dairy=dairy,completed=False,due_date=now)
	pregnant_list = Pregnant.objects.filter(pregnancy__cow__dairy=dairy,completed=False,due_date=now)
	treatment_list = Treatment.objects.filter(pregnancy__cow__dairy=dairy,completed=False,due_date=now)

	result_list = sorted(chain(conceieved_list,parturition_list,pregnant_list,treatment_list,heat_list,todays_tasks),key=attrgetter('due_date'))



	return render_to_response('alerts/alerts.html',
                          {'alerts':notifications,'result_list':result_list,'date':now,'next':next_date,'previous':previous_date},
                          context_instance=RequestContext(request))

def cow_status(request):
	dairy = UserDairy.objects.get(user=request.user).dairy
	parts = Parturition.objects.filter(pregnancy__cow__dairy=dairy,completed=False)
	fresh = []
	ready = []
	preg_one = []
	preg_two = []
	preg_three = []
	dry_preg = []
	closeup = []

	now = datetime.date(datetime.now())
	for part in parts:
		days = (now - part.date)
		if days.days < 30 :
			data = {}
			data['cow'] = part.pregnancy.cow
			data['stage'] = part
			fresh.append(data)
		if days.days > 30:
			data = {}
			data['cow'] = part.pregnancy.cow
			data['stage'] = part

			ready.append(data)
        pregs = Pregnant.objects.filter(pregnancy__cow__dairy=dairy,completed=False)		
	for preg in pregs:
		days = now - preg.date	
		if days.days <= 5:
			data = {}
			data['cow'] = preg.pregnancy.cow
			data['stage'] = preg
			preg_one.append(data)
		if days.days >60 and days.days <90:
			data = {}
			data['cow'] = preg.pregnancy.cow
			data['stage'] = preg
	

			preg_two.append(data)
		if days.days >=90 and days.days <210:

			data = {}
			data['cow'] = preg.pregnancy.cow
			data['stage'] = preg
	
			preg_three.append(data)
		if days.days >=210 and days.days <240:
			data = {}
			data['cow'] = preg.pregnancy.cow
			data['stage'] = preg
	
			dry_preg.append(data)
		if days.days >=210:
			data = {}
			data['cow'] = preg.pregnancy.cow
			data['stage'] = preg
	
			closeup.append(data)
		
		
	return render_to_response('alerts/status.html',
                          {'fresh':fresh,'ready':ready,'preg_one':preg_one,'preg_two':preg_two,'preg_three':preg_three,'dry_preg':dry_preg,'closeup':closeup},
                          context_instance=RequestContext(request))



def add_mobile_number(request):
	dairy = UserDairy.objects.get(user=request.user).dairy
	form = MobileNumberForm()
	if request.method=="POST":
		form = MobileNumberForm(data=request.POST)
		if form.is_valid():
			number = form.save(commit=False)
			number.dairy = dairy
			number.save()
			return HttpResponseRedirect('/settings/')	
	return render_to_response('alerts/add_mobile_number.html',
                          {'form':form},
                          context_instance=RequestContext(request))


