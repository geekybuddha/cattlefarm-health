# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Schedule'
        db.create_table(u'vaccination_schedule', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('state', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('disease', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('animal', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('vaccine', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('dose', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('immunity', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('time_of_vaccination', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'vaccination', ['Schedule'])


    def backwards(self, orm):
        # Deleting model 'Schedule'
        db.delete_table(u'vaccination_schedule')


    models = {
        u'vaccination.schedule': {
            'Meta': {'object_name': 'Schedule'},
            'animal': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'disease': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'dose': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'immunity': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'time_of_vaccination': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'vaccine': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['vaccination']