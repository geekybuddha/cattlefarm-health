from celery.decorators import task

from cow.models import HealthTask,Dairy,send_sms,UserDairy,Notifications
from django.core.mail import send_mail
from datetime import datetime
from pregnancy.models import *
from django.core.mail import EmailMultiAlternatives
from alerts.models import Alert, MobileNumber


def send_message(body,dairy):
	try:
		if dairy.user.notifications.sms_notifications == True:
			mobile = dairy.user.notifications.mobile_number
			send_sms(mobile,body)
	except Notifications.DoesNotExist:
		print "user notifications not initialized"
	
	numbers = MobileNumber.objects.filter(dairy=dairy)
	for mobile_data in numbers:
		if mobile_data.is_subscribed:
			send_sms(mobile_data.number,body)	

        	



@task
def todays_tasks_reminder():
    dairys = Dairy.objects.all()
    for dairy in dairys:
	due_tasks = HealthTask.objects.filter(due_date=datetime.now(),task_completed=False,cow__dairy=dairy).order_by('due_date')
    	if due_tasks:
		if dairy.user.notifications.email_notifications == True:
			tasks_list = ""
			for task in due_tasks:
				tasks_list = tasks_list+"<li><a href='http://dairyhealth.cu.cc/task/%s/'>%s</a> for cow <a href='http://dairyhealth.cu.cc/cow/%s/'>%s</a></li>" % (task.id,task.task_name,task.cow.id,task.cow.name)
			tasks_list = "<ul>"+tasks_list+"</ul>"
    			html_body = "Hi <br><br> Your have following health tasks scheduled for today- <br><br> %s <br><br> Cheers"% (tasks_list)	
    			
    			body = "Hi, Your have following health task for today and need urgent attention- %s  Cheers"% (tasks_list)	
			to = [dairy.user.email,"ritu.ecb@gmail.com","shreekant@ringad.net",]
    			subject = "DairyHealth - Today's Health Task"
    			msg = EmailMultiAlternatives(subject, body, "info@dairyhealth.cu.cc", to)
    			msg.attach_alternative(html_body, "text/html") 
    			#msg.send()
		tasks_list = ""
		for task in due_tasks:
			tasks_list = tasks_list+"%s for cow - %s," % (task.task_name,task.cow.name)
		body = "Today's health tasks - %s  Cheers"% (tasks_list)	
		send_message(body,dairy)	

@task
def pending_tasks_reminder():
    print "im ahere"
    dairys = Dairy.objects.all()
    for dairy in dairys:
	due_tasks = HealthTask.objects.filter(due_date__lt=datetime.now(),task_completed=False,cow__dairy=dairy).order_by('due_date')
    	if due_tasks:
		try:
			if dairy.user.notifications.email_notifications == True:
				tasks_list = ""
				for task in due_tasks:
					tasks_list = tasks_list+"<li><a href='http://dairyhealth.cu.cc/task/%s/'>%s</a> for cow <a href='http://dairyhealth.cu.cc/cow/%s/'>%s</a></li>" % (task.id,task.task_name,task.cow.id,task.cow.name)
				tasks_list = "<ul>"+tasks_list+"</ul>"
				html_body = "Hi <br><br> Your have following health tasks over due and need urgent attention- <br><br> %s <br><br> Cheers"% (tasks_list)	
				
				body = "Hi, Your have following health tasks over due and need urgent attention- %s  Cheers"% (tasks_list)	
				to = [dairy.user.email,"ritu.ecb@gmail.com","shreekant@ringad.net",]
				subject = "DairyHealth - Due date passed tasks need urgent attention"
				msg = EmailMultiAlternatives(subject, body, "info@dairyhealth.cu.cc", to)
				msg.attach_alternative(html_body, "text/html") 
				#msg.send()
		except Notifications.DoesNotExist:
			print "user has no mobile number attached"
		tasks_list = ""
		for task in due_tasks:
			tasks_list = tasks_list+"%s for cow - %s," % (task.task_name,task.cow.name)
		if due_tasks.count() > 5:
			body = "Due tasks need urgent attention: You have more than 5 due tasks. Please complete and mark as soon as possible. "
		else:
			body = "Due Tasks need urgent attention- %s  Cheers"% (tasks_list)	
		send_message(body,dairy)	

@task
def send_alerts():
	dairies = Dairy.objects.all()
	for dairy in dairies:
		now = datetime.date(datetime.now())
		
			
		heat_notifications = []
		part_notifications = []
		con_notifications = []
		preg_notifications = []
		'''
		heat alerts
		'''
		heat_cows = Heat.objects.filter(pregnancy__cow__dairy=dairy,completed=False)
		heat_alerts = Alert.objects.filter(stage="heat")
		if heat_alerts:
			for heat in heat_cows:
				days = now - heat.date

				for alert in heat_alerts:
					if days.days == alert.day:
						data = {}
						data['alert'] = alert
						data['cow'] = heat.pregnancy.cow
						data['stage'] = heat
						heat_notifications.append(data)
			tasks_list = ""
			for task in heat_notifications:
				tasks_list = tasks_list+"%s for Cattle - %s," % (task['alert'].message,task['cow'].name)
				body = "Heat related critical alerts - %s  "% (tasks_list)	
			send_message(body,dairy)	
							
		'''
		partution alerts
		'''
		part_cows = Parturition.objects.filter(pregnancy__cow__dairy=dairy,completed=False)
		part_alerts = Alert.objects.filter(stage="parturition")
		if part_alerts:
			for heat in part_cows:
				days = now - heat.date	
				for alert in part_alerts:
					if days.days == alert.day:
						data = {}
						data['alert'] = alert
						data['cow'] = heat.pregnancy.cow
						data['stage'] = heat
						part_notifications.append(data)

			tasks_list = ""
			for task in part_notifications:
				tasks_list = tasks_list+"%s for Cattle - %s," % (task['alert'].message,task['cow'].name)
				body = "Parturition related critical alerts - %s  "% (tasks_list)	
			send_message(body,dairy)

		
		'''
		conceivevd alerts
		'''
		con_cows = Conceieved.objects.filter(pregnancy__cow__dairy=dairy,completed=False)
		con_alerts = Alert.objects.filter(stage="conceived")
		if con_alerts:
			for heat in con_cows:
				days = now - heat.date
				for alert in con_alerts:
					if days.days == alert.day:

						data = {}
						data['alert'] = alert
						data['cow'] = heat.pregnancy.cow
						data['stage'] = heat

						con_notifications.append(data)
			tasks_list = ""
			for task in con_notifications:
				tasks_list = tasks_list+"%s for Cattle - %s," % (task['alert'].message,task['cow'].name)
				body = "Conceieved related critical alerts - %s  "% (tasks_list)	
			send_message(body,dairy)


		'''
		pregnant alerts
		'''
		preg_cows = Pregnant.objects.filter(pregnancy__cow__dairy=dairy,completed=False)
		preg_alerts = Alert.objects.filter(stage="pregnant")

		if preg_alerts:
			for heat in preg_cows:
				days = now - heat.date	
				for alert in preg_alerts:
					if days.days == alert.day:
						data = {}
						data['alert'] = alert
						data['cow'] = heat.pregnancy.cow
						data['stage'] = heat
						preg_notifications.append(data)
		
			tasks_list = ""
			for task in preg_notifications:
				tasks_list = tasks_list+"%s for Cattle - %s," % (task['alert'].message,task['cow'].name)
				body = "Pregnancy related critical alerts - %s  "% (tasks_list)	
			send_message(body,dairy)	



		

